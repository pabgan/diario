#!/bin/zsh

##########################
# CONFIGURATION
# TODO: get configuration from a configuration file?
# TODO: make it XDG compliant??
printer=${DIARIO_PRINTER:-md2term.sh}
diario_dir=${DIARIO_DIR:-$HOME/.local/share/diario}
setopt EXTENDED_GLOB

##########################
# CONSTANTS
help_message="Syntax is: $(basename $0) [--verbose] action [...]

              action is one of: add, ls, show, edit"

##########################
# ACTIONS
add() {
	if [[ $o_verbose ]]; then echo ">DEBUG> $0 $@" >/dev/stderr ; fi

	# TODO: remove if commenting this line does not give problems in a while
	#log_date=$(date +"%Y-%m-%d %H:%m:%S")
	filename=$(date +"%Y-%m-%dT%H%M").md
	filepath="${diario_dir}"/${filename}

	if [[ $o_verbose ]]; then echo ">DEBUG> Creating file: $filepath" >/dev/stderr ; fi


	#echo ${template} > $filepath

	# If user wants to add the contents of a file directly
	if [[ -e $1 ]]; then
		if [[ $o_verbose ]]; then echo ">DEBUG> Feeding text of $1 into file" >/dev/stderr ; fi
		cat $1 > $filepath
	# or he wants to open the editor
	elif [[ -z $1 ]]; then
		if [[ $o_verbose ]]; then echo ">DEBUG> Opening default editor" >/dev/stderr ; fi
		$EDITOR $filepath
	# or else he wants to log some text written as a parameter directly
	else
		if [[ $o_verbose ]]; then echo ">DEBUG> Feeding text into file: ${@}" >/dev/stderr ; fi
		echo ${@} > $filepath
	fi

	# If something was effectively written to the log
        if [[ -s $filepath ]]
        then
		# Print the path to the file
                echo $filepath
        else
		# Else remove the created file
                rm $filepath
        fi
}

ls() {
	if [[ $o_verbose ]]; then echo ">DEBUG> $0 $@" >/dev/stderr ; fi

	zparseopts -F -D -files-without-match=o_files_without_match

	search_terms=($@)
	if [[ $o_verbose ]]; then echo ">DEBUG> Filtering search results with: $files_days_filter" >/dev/stderr ; fi
	files_filtered="$diario_dir/${files_days_filter}*.md"
	if [[ $o_verbose ]]; then echo ">DEBUG> Files globbing: ${(f)files_filtered}" >/dev/stderr ; fi
	files_filtered=$(eval /bin/ls $o_reverse ${(f)files_filtered} 2>/dev/null)
	if [[ $o_verbose ]]; then echo ">DEBUG> Files filtered by days: ${(f)files_filtered}" >/dev/stderr ; fi

	if [[ -n ${files_filtered} ]]; then
		for search_term in $search_terms; do
			if [[ $o_verbose ]]; then echo ">DEBUG> Searching for '${search_term}' in ${(f)files_filtered}" >/dev/stderr ; fi
			files_filtered=$(eval grep ${o_files_without_match:=--files-with-matches} -iE '${search_term/+/\\+}' ${(f)files_filtered})
			if [[ -z ${files_filtered} ]]; then
				if [[ $o_verbose ]]; then echo ">DEBUG> No files left to search in." >/dev/stderr ; fi
				break
			fi
		done
	fi

	# In case there was no search_terms we need to expand
	# the files_days_filter globbing
	if [[ $o_verbose ]]; then echo ">DEBUG> Files passing all filters:\n${files_filtered}" >/dev/stderr ; fi
	echo ${files_filtered}
}

show() {
	if [[ $o_verbose ]]; then echo ">DEBUG> $0 $@" >/dev/stderr ; fi
	files_to_show=($(ls $@))
	if [[ $o_verbose ]]; then echo ">DEBUG> Files to show: $files_to_show" >/dev/stderr ; fi
	for f in ${files_to_show[@]}; do
		if [[ $o_verbose ]]; then echo ">DEBUG> Sending file to print: $f" >/dev/stderr ; fi
		print_file $f
	done
}

edit() {
	if [[ $o_verbose ]]; then echo ">DEBUG> $0 $@" >/dev/stderr ; fi

	files_to_edit=$(ls $@ | fzf --multi --tac)
	$EDITOR $(echo $files_to_edit)

	# Remove files which content was deleted completely
	for file in $(echo $files_to_edit); do
		if [[ ! -s $file ]]; then
			rm $file
		else
			echo $file
		fi
	done
}

##########################
# Auxiliary functions
#
print_file() {
	if [[ $o_verbose ]]; then echo ">DEBUG> $0 $@" >/dev/stderr ; fi
	to_print=""
	file_to_print=$1
	year='[0-9][0-9][0-9][0-9]'
	month='[0-1][0-9]'
	day='[0-3][0-9]'
	hour='[0-2][0-9]'
	minute='[0-5][0-9]'

	header=$(basename $file_to_print '.md' | sed -E "s/(${year}-${month}-${day})T($hour)($minute)/\1 at \2:\3/")

	#to_print="[${f##*/}]($f)\n" # print link to file in markdown format
	to_print="${to_print}\n---\n"
	# If requested print link to file in markdown format
	if [[ ${o_show_file_names} ]]; then
		to_print="${to_print}\n    [${f##*/}]($f)" >&2;
	fi
	to_print="${to_print}\nOn $header:\n"
	to_print="${to_print}\n$(cat $f)\n"
	#echo $to_print | ${printer}
	echo $to_print | ${(z)printer}
}

set_filter_days() {
	files_days_filter="($(date --iso-8601)"
	# This function contructs a globbing regex that looks like
	#     "(2022-03-16|2022-03-15)"
	if [[ $days > 0 ]]; then

		for d in {1..$days}; do
			day=$(date  --date "$d days ago" --iso-8601)
			files_days_filter="$files_days_filter|$day"
		done
	fi
	files_days_filter="$files_days_filter)"
}

##########################
# PARSE INPUT

zparseopts -F -D -verbose=o_verbose -reverse=o_reverse

if [[ $# < 1 ]]; then
	echo ">ERROR> invalid syntax." >/dev/stderr
	echo "$help_message" >/dev/stderr
	exit 2
fi


if [[ $o_verbose ]]; then echo ">DEBUG> Verbose mode" >/dev/stderr ; fi

action=$1
shift

zparseopts -F -D -days:=o_days -file-names=o_show_file_names
days="${o_days[2]}"

##########################
# 3. MAIN
if [[ $days ]]; then
	set_filter_days
fi

$action "$@"
