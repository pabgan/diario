#!/bin/zsh
# TODO: is there a better way to do this?
output=$(cat "${1:-/dev/stdin}" )

echo $output | pandoc -f markdown -t html | lynx -stdin -dump --hiddenlinks=ignore
