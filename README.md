# diario.sh

Another journal in the shell, written in ZSH.

Plain text, UNIX philosophy, easy and simple. Inspired by [todo.txt](http://todotxt.org/).


# Installation

Just drop the script wherever you want that is in your `$PATH`.


## Dependencies
You need installed the following:

- `zsh`, to run it.
- `fzf`, to select logs to `edit`.
- _optionally_ `md2term.sh`, to `show` logs. And thus `pandoc` and `lynx`.

# How to use it

## Setup

You must set two variables by your own means (`~/.bashrc`, `~/.zshrc`...):

- `DIARIO_PRINTER`: it defaults to `md2term.sh` if not set. This printer must accept input from `stdin`.
- `DIARIO_DIR`: it defaults to `$HOME/.local/share/diario/` if not set.


## Create new log

```sh
pabgan@manoliTo ~
  $ diario.sh add '# I did that

Because of this @test.'
/home/pabgan/work/organizacion/diario/2022-03-15T17:23:30+01:00.md
```
The output is the created file's path.


## Search into logs

```sh
pabgan@manoliTo ~
  $ diario.sh ls @test
/home/pabgan/work/organizacion/diario/2021-10-25T15:22:30+02:00.md
/home/pabgan/work/organizacion/diario/2022-03-11T09:56:56+01:00.md
```

There is no need for the `@`. I just happen to use `@` and `+` with meanings akin to `todo.txt`.

- Limit how far in the past to search

```sh
pabgan@manoliTo ~
  $ diario.sh --days 5 ls @test
/home/pabgan/work/organizacion/diario/2022-03-11T09:56:56+01:00.md
```

This option also works with `show` and `edit` commands.

## Show logs

```sh
pabgan@manoliTo ~
  $ diario.sh show @test

   /home/pabgan/work/organizacion/diario/2021-10-25T15:22:30+02:00.md

   On 2021-10-25 15:22:30:

                                   I did that

   Because of this @test.
     __________________________________________________________________
   /home/pabgan/work/organizacion/diario/2022-03-11T09:56:56+01:00.md

   On 2022-03-11 09:56:56:

                      @revision de tickets +generic +21.9.

+DSLE-15478 - Calix : E7 : Vectoring

   Es un holder para otros tres tickets: +DSLE-23347, +DSLE-23406 y
   +DSLE-23405. El primero ya lo ha cerrado @adelacruz, y los otros dos
   los tiene asignados @ialmandoz para @test_in_production.
     __________________________________________________________________

```

## Edit logs

```sh
pabgan@manoliTo ~
  $ diario.sh edit @test
  /home/pabgan/work/organizacion/diario/2022-03-11T09:56:56+01:00.md
> /home/pabgan/work/organizacion/diario/2021-10-25T15:22:30+02:00.md
  2/2 (0)
>
╭────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ ───────┬────────────────────────────────────────────────────────────────────────                                           │
│        │ File: /home/pabgan/work/organizacion/diario/2021-10-25T15:22:30+02:00.md                                          │
│ ───────┼────────────────────────────────────────────────────────────────────────                                           │
│    1   │ # I did that                                                                                                      │
│    2   │                                                                                                                   │
│    3   │ Because of this @test.                                                                                            │
│ ───────┴────────────────────────────────────────────────────────────────────────                                           │

```
All log entries that contain the search term will be shown in FZF for you to select those to edit.

The search term is used in a `grep` search, so you can do things like:

```sh
pabgan@manoliTo ~
  $ diario.sh edit '@test|auto'
```

# tips

- Navigate through the journal: use [ranger](https://ranger.github.io/)... or whatever

```sh
alias diario_navigate='ranger $DIARIO_DIR'
```

- Create an entry with some kind of template, but delete it if resulting file is empty:

```sh
diario_task() {
	export_task_variable_if_need_be
	file=$(diario.sh add "# +$TASK - $1\n\n## $(cat .title)")
	$EDITOR $file

	# If file is left empty, user probably regreted
	# logging anything so let's just delete the file
	# and pretend nothing happend
	if [[ ! -s $file ]]; then
		rm $file
	else
		echo $file
	fi
}
alias dt='diario_task'
```

- Always open the editor to write the log:

```sh
alias da='$EDITOR $(diario.sh add)'
```

